"use strict";

const cp = require("child_process");

console.log({commits: process.argv[2]})

const commits = JSON.parse(process.argv[2] || "[]");

console.log({parsedCommits: commits})

const rebasePr = () => {
  console.log("\nChecking if rebase is required...");
  try {
    console.log("Rebasing on top of master...");
    for (const commit of commits.reverse()) {
      cp.execSync(`git rebase ${commit} HEAD`)
    }
    cp.execSync("git rebase origin/master", { stdio: "inherit" });
    cp.execSync(`git log --oneline -${++commits.length}`, {stdio: 'inherit'})
    console.log("Rebase successful.");
  } catch (err) {
    console.log("Failed to rebase on top of master.");
    console.error(err);
    process.exit(1);
  }
};

rebasePr();
